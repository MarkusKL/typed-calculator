module Stack
( StackD
, Stack
, push
, push1
, push2
, pop
, pop1
, pop2
, moveWhile
, moveRest
, stack
, stackD
) where

import Control.Monad.State.Lazy
import Control.Applicative ((<|>))

type StackD s1 s2 a = StateT ([s1],[s2]) Maybe a
type Stack s a = StackD s () a

stack :: Stack s a -> [s] -> Maybe (a,[s])
stack s ss = stackD s (ss,[]) >>= \(a,(arr,_)) -> Just (a,arr)

stackD :: StackD s1 s2 a -> ([s1],[s2]) -> Maybe (a,([s1],[s2]))
stackD = runStateT

split1 :: ([a],[b]) -> Maybe (a,([a],[b]))
split1 (a:as,bs) = Just (a,(as,bs))
split1 ([], _) = Nothing

split2 :: ([a],[b]) -> Maybe (b,([a],[b]))
split2 (as,b:bs) = Just (b,(as,bs))
split2 (_, []) = Nothing

pop1 :: StackD s1 s2 s1
pop1 = StateT split1

pop2 :: StackD s1 s2 s2
pop2 = StateT split2

pop :: Stack s s
pop = pop1

push1 :: s1 -> StackD s1 s2 ()
push1 s = StateT $ \(s1,s2) -> Just ((),(s:s1,s2))

push2 :: s2 -> StackD s1 s2 ()
push2 s = StateT $ \(s1,s2) -> Just ((),(s1,s:s2))

push :: s -> Stack s ()
push = push1

moveWhile :: (s1 -> Bool) -> (s1 -> s2) -> StackD s1 s2 ()
moveWhile p t = moveWhile' p t <|> return ()

moveWhile' :: (s1 -> Bool) -> (s1 -> s2) -> StackD s1 s2 ()
moveWhile' p t = pop1 >>= \a -> if p a
    then push2 (t a) >> moveWhile p t
    else push1 a

moveRest :: (s1 -> s2) -> StackD s1 s2 ()
moveRest = moveWhile (const True)
