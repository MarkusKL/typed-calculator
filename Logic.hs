module Logic where

import Control.Applicative ((<*>),(<$>))
import Control.Monad (void)

import Stack

data Associativity = AssLeft | AssRight deriving (Eq)
type Precedence = Integer

data Token = Number Double | Operator Operator deriving Show
type Tokens = [Token]
type RPN = [Token]

data Direction = L | R deriving (Show, Eq)
data Operator = Plus | Multiply | Power | Minus | Division | Par Direction deriving (Show, Eq)

apply :: Operator -> Double -> Double -> Double
apply o = case o of
    Plus -> (+)
    Multiply -> (*)
    Power -> (**)
    Minus -> (-)
    Division -> (/)
    Par _ -> const

precedence :: Operator -> Integer
precedence o = case o of
    Plus -> 6
    Multiply -> 7
    Power -> 8
    Minus -> 6
    Division -> 7
    Par _ -> 0

associativity :: Operator -> Associativity
associativity o = case o of
    Plus -> AssLeft
    Multiply -> AssLeft
    Power -> AssLeft
    Minus -> AssLeft
    Division -> AssLeft
    Par _ -> AssLeft

instance Ord Operator where
    compare o1 o2 = if o1 == o2
        then EQ
        else if associativity o1 == AssLeft
            then if precedence o1 <= precedence o2 then LT else GT
            else if precedence o1 < precedence o2 then LT else GT

calculate :: Tokens -> Either String Double
calculate ts = (shuntingYard . insertImplicit . surround) ts >>= calculateRPN

calculateRPN :: RPN -> Either String Double
calculateRPN rpn = case stack (mapM_ calculateRPN' rpn >> pop) [] of
    Just (d,[]) -> Right d
    Just _ -> Left "RPN: Leftover tokens"
    Nothing -> Left "RPN: Missing tokens"

calculateRPN' :: Token -> Stack Double ()
calculateRPN' t = case t of
    Number d -> push d
    Operator o -> flip (apply o) <$> pop <*> pop >>= push

surround :: Tokens -> Tokens
surround ts = Operator (Par L) : surround' ts
    where
        surround' [] = Operator (Par R) : []
        surround' (x:xs) = x : surround' xs

insertImplicit :: Tokens -> Tokens
insertImplicit [] = []
insertImplicit (Number n : Operator (Par L) : ts) = Number n : Operator Multiply : insertImplicit (Operator (Par L) : ts)
insertImplicit (Operator (Par L) : Operator Minus : ts) = Operator (Par L) : Number 0 : Operator Minus : insertImplicit ts
insertImplicit (t:ts) = t: insertImplicit ts

shuntingYard :: Tokens -> Either String RPN
shuntingYard ts = shuntingError $ fmap (reverse . snd . snd) (
    stackD (mapM_ shuntingYard' ts >> moveRest Operator) ([],[]))

shuntingError :: Maybe a -> Either String a
shuntingError (Just a) = Right a
shuntingError Nothing = Left "Shunting yard error"

shuntingYard' :: Token -> StackD Operator Token ()
shuntingYard' t = case t of
    n@(Number _) -> push2 n
    Operator (Par L) -> push1 (Par L)
    Operator (Par R) -> moveWhile (Par L /=) Operator >> void pop1
    Operator o1 -> moveWhile (o1 <=) Operator >> push1 o1
