import System.IO

import Calculator

configure :: IO ()
configure = do
    hSetBuffering stdin LineBuffering
    hSetBuffering stdout NoBuffering

main :: IO ()
main = do
    configure
    loop

loop :: IO ()
loop = do
    putStr "=> "
    expr <- getLine
    if expr == "exit"
        then return ()
        else do
            let res = calculateExpession expr
            putStrLn . (++ "\n") . (either (id) (show)) $ res
            loop
