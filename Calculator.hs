module Calculator where

import Control.Applicative (many, (<|>), (<$), (<$>))

import Logic
import Parser

calculateExpession :: Expression -> Either String Double
calculateExpession = (>>= calculate) . parse parseTokens

parseOp :: Char -> Operator -> Parser Operator
parseOp c o = o <$ char c

parseAnyOperator :: Parser Operator
parseAnyOperator = parseOp '+' Plus
               <|> parseOp '*' Multiply
               <|> parseOp '^' Power
               <|> parseOp '-' Minus
               <|> parseOp '/' Division
               <|> parseOp '(' (Par L)
               <|> parseOp ')' (Par R)

parseToken :: Parser Token
parseToken = Number <$> double <|> Operator <$> parseAnyOperator

parseTokens :: Parser Tokens
parseTokens = many . wrap $ parseToken
