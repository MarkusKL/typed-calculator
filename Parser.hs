module Parser where

import Data.Char
import Control.Applicative
import Control.Monad (liftM2,void)

type Expression = String
type ParseError = String

data Parser t = Parser {runParser :: Expression -> Either ParseError (t,Expression)}

instance Functor Parser where
    fmap f (Parser r) = Parser $ fmap (\(a, expr2) -> (f a, expr2)) . r

instance Applicative Parser where
    pure a = Parser $ \expr -> Right (a,expr)
    Parser rf <*> Parser r = Parser $ \expr -> do
        (f, expr2) <- rf expr
        (a, expr3) <- r expr2
        return (f a, expr3)

instance Alternative Parser where
    empty = Parser $ \_ -> Left "Alternative empty error"
    Parser pa <|> Parser pb = Parser $ \expr -> case pa expr of
        Left _ -> pb expr
        Right r -> Right r
    some p = liftM2 (:) p (many p)
    many p = some p <|> return []

infix 1 <?>
(<?>) :: Parser a -> String -> Parser a
p <?> s = p <|> throwError s

instance Monad Parser where
    return = pure
    Parser r >>= f = Parser $ \expr -> do
        (a, expr2) <- r expr
        let Parser r2 = f a
        r2 expr2

parse :: Parser p -> Expression -> Either ParseError p
parse (Parser r) expr = case r expr of
    Left e          -> Left e
    Right (t,[])    -> Right t
    Right (_, x:_) -> Left $ "Did not expect '" ++ [x] ++ "'"

throwError :: ParseError -> Parser a
throwError e = Parser $ \_ -> Left e

check :: Parser Char
check = Parser tCopy

consume :: Parser Char
consume = Parser tSplit

consumeIf :: Char -> Parser Bool
consumeIf c = do
    x <- check
    if x == c
        then do
            void consume
            return True
        else return False

satisfy :: (Char -> Bool) -> Parser Char
satisfy f = do
    x <- consume
    if f x
        then return x
        else throwError $ "Did not expect '" ++ [x] ++ "'"

space :: Parser ()
space = void . many $ satisfy isSpace

wrap :: Parser a -> Parser a
wrap p = do
    space
    a <- p
    space
    return a

double :: Parser Double
double = ((parseRead . some) $ satisfy isNumber <|> char '.') <?> "Failed to parse number"

char :: Char -> Parser Char
char c = satisfy (==c)

tCopy :: Expression -> Either ParseError (Char,Expression)
tCopy [] = Left "Expected any character but found none"
tCopy xs@(x:_) = Right (x,xs)

tSplit :: Expression -> Either ParseError (Char,Expression)
tSplit [] = Left "Expected any character but found none"
tSplit (x:xs) = Right (x,xs)

parseRead :: Read a => Parser String -> Parser a
parseRead p = do
    x <- p
    case readMay x of
        Just a -> return a
        Nothing -> throwError "Read failed"

readMay :: Read a => String -> Maybe a
readMay s = case [x | (x,t) <- reads s, ("","") <- lex t] of
    [x] -> Just x
    _   -> Nothing
